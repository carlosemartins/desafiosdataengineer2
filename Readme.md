URL para acesso e testes

http://testefacil.tk/bigdata/

Perguntas

1. O que você faria caso quisesse obter essas informações de forma recorrente, ou seja, todo dia?

R: Criar uma tarefa agendada (cron) e armazenaria estes valores em um banco de dados (mysql por exemplo)

2. Como você validaria se as respostas obtidas do crawler estão corretas ou não?

R: Geraria um schema e compararia com o que foi obtido no site

3. O que você faria se tivesse mais tempo para resolver o desafio?

R: Tentaria adicionar as estações que retornam menos dados ou dados diferentes, e faria uma validação melhor nos dados vazios ou nulo

4. Como você resolveria esse desafio e/ou as perguntas caso tivesse acesso aos recursos da Amazon Web Services, Azure ou Google Cloud?

R: Daria para criar processos e threads que facilitariam a aquisição dos dados. (ex: criar uma chamada assincrona diferente pra cada estação e no final juntar tudo)